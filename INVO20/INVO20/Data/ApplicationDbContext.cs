﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using INVO20.Data;

namespace INVO20.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Worker> Workers { get; set; }
        public DbSet<WorkersGroup> WorkersGroups { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BlockDepend>()
                .HasKey(bc => new { bc.BlockID, bc.DependsID });
            modelBuilder.Entity<BlockDepend>()
                .HasOne(bc => bc.Blocks)
                .WithMany(b => b.Blocks)
                .HasForeignKey(bc => bc.BlockID)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<BlockDepend>()
                .HasOne(bc => bc.DependsOn)
                .WithMany(c => c.DependsOn)
                .HasForeignKey(bc => bc.DependsID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<HourPkt>()
                .HasKey(bc => new { bc.WorkerID, bc.TaskID });
            modelBuilder.Entity<HourPkt>()
                .HasKey(bc => new { bc.ID });
            modelBuilder.Entity<HourPkt>()
                .HasIndex(bc => new { bc.ID });
            modelBuilder.Entity<HourPkt>()
                .HasOne(hp => hp.Task)
                .WithMany(t => t.Hourpkts)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<HourPkt>()
                .HasOne(hp => hp.Worker)
                .WithMany(w => w.Hourpkts)
                .OnDelete(DeleteBehavior.NoAction);
        }

        public DbSet<INVO20.Data.HourPkt> HourPkt { get; set; }
    }
}
