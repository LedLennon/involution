﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace INVO20.Data
{
    public class Worker
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ID { get; set; }

        [MaxLength(64)]
        [Display(Name = "Nome dell'impiegato")]
        [Required]
        public string Name { get; set; }

        [MaxLength(3)]
        [Display(Name = "Acronimo (3 lettere)")]
        [Required]
        public string Acronimo { get; set; }

        [ForeignKey("Area")]
        [Display(Name = "Area di Competenza")]
        public int AreaID { get; set; }
        public Area Area { get; set; }

        [ForeignKey("WorkersGroup")]
        [Display(Name = "Reparto di appartenenza")]
        public int WorkerGroupID { get; set; }
        public WorkersGroup WorkersGroup { get; set; }

        [Display(Name = "Pacchetti di ore spesi dall'impiegato sul task")]
        public virtual ICollection<HourPkt> Hourpkts { get; set; }
    }
}
