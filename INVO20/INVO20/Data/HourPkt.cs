﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace INVO20.Data
{
    public class HourPkt
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ID { get; set; }

        [ForeignKey("Worker")]
        [Display(Name = "Persona alla quale è assegnato il pacchetto di ore")]
        public int WorkerID { get; set; }
        public Worker Worker { get; set; }

        [Display(Name = "Commento")]
        [MaxLength(128)]
        public string Descrizione { get; set; }

        [Display(Name = "Giorno")]
        public DateTime Date { get; set; }

        [Display(Name = "Numero di ore spese sul task nel dato giorno")]
        public double Quantity { get; set; }

        [ForeignKey("Task")]
        [Display(Name = "Task al quale è assegnato il pacchetto di ore")]
        public int TaskID { get; set; }
        public Task Task { get; set; }
    }
}
