﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace INVO20.Data
{
    public class Task
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ID { get; set; }

        [MaxLength(128)]
        [Display(Name = "Nome Sintetico del task")]
        [Required]
        public string Name { get; set; }

        [ForeignKey("Project")]
        [Display(Name = "Progetto principale al quale afferisce il task")]
        public int ProjectID { get; set; }
        public Project Project { get; set; }

        [Display(Name = "Livello del task (Task principale => Livello 0")]
        public int Level { get; set; }

        [Display(Name = "Ore assegnate")]
        public double AssignedHours { get; set; }

        [ForeignKey("SuperTask")]
        [Display(Name = "Riferimento al task di livello superiore")]
        public int? TaskID { get; set; }
        public Task SuperTask { get; set; }

        [ForeignKey("TaskID")]
        [ScaffoldColumn(false)]
        public ICollection<Task> SubTasks { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Descrizione")]
        [MaxLength(1280)]
        public string Descrizione { get; set; }

        [Display(Name = "Task che vengono bloccati")]
        public virtual ICollection<BlockDepend> Blocks { get; set; }

        [Display(Name = "Dipendenza da altri task")]
        public virtual ICollection<BlockDepend> DependsOn { get; set; }

        [ForeignKey("Worker")]
        [Display(Name = "Persona alla quale è assegnato il task")]
        public int WorkerID { get; set; }
        public Worker Worker { get; set; }

        [Display(Name = "Pacchetti di ore spesi sul task")]
        public virtual ICollection<HourPkt> Hourpkts { get; set; }
    }

    public class BlockDepend
    {
        [ForeignKey("Blocks")]
        public int BlockID { get; set; }
        public Task Blocks { get; set; }

        [ForeignKey("DependsOn")]
        public int DependsID { get; set; }
        public Task DependsOn { get; set; }
    }
}
