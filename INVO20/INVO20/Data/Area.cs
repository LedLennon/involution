﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace INVO20.Data
{
    public class Area
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ID { get; set; }

        [MaxLength(32)]
        [Display(Name = "Nome Area")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Impiegati appartenenti a questa area")]
        public virtual ICollection<Worker> Workers { get; set; }

    }
}
