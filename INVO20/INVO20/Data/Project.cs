﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace INVO20.Data
{
    public class Project
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ID { get; set; }

        [MaxLength(128)]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Nome (Codice) Commessa")]
        [MaxLength(64)]
        public string Commessa { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Descrizione")]
        [MaxLength(1280)]
        public string Descrizione { get; set; }

        [Display(Name = "Data di consegna")]
        public DateTime DueDate { get; set; }

        [Display(Name = "Data di inizio")]
        public DateTime StartDate { get; set; }

        [ScaffoldColumn(false)]
        public ICollection<Task> Tasks { get; set; }
    }
}
