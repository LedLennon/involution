﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace INVO20.Data
{
    public class WorkersGroup
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ID { get; set; }

        [MaxLength(64)]
        [Display(Name = "Nome Reparto")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Impiegati appartenenti a questo Gruppo")]
        public virtual ICollection<Worker> Workers { get; set; }
    }
}
