﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using INVO20.Data;

namespace INVO20.Pages.Projects
{
    public class IndexModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public IndexModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Project> Project { get;set; }

        public async System.Threading.Tasks.Task OnGetAsync()
        {
            Project = await _context.Projects.ToListAsync();
        }
    }
}
