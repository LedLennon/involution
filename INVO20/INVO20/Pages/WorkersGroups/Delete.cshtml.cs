﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using INVO20.Data;

namespace INVO20.Pages.WorkersGroups
{
    public class DeleteModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public DeleteModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public WorkersGroup WorkersGroup { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WorkersGroup = await _context.WorkersGroups.FirstOrDefaultAsync(m => m.ID == id);

            if (WorkersGroup == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WorkersGroup = await _context.WorkersGroups.FindAsync(id);

            if (WorkersGroup != null)
            {
                _context.WorkersGroups.Remove(WorkersGroup);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
