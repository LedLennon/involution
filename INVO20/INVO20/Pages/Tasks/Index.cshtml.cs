﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using INVO20.Data;

namespace INVO20.Pages.Tasks
{
    public class IndexModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public IndexModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Data.Task> Task { get;set; }

        public async System.Threading.Tasks.Task OnGetAsync()
        {
            Task = await _context.Tasks
                .Include(t => t.Project)
                .Include(t => t.SuperTask)
                .Include(t => t.Worker).ToListAsync();
        }
    }
}
