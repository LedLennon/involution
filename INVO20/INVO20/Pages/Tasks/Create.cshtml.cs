﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using INVO20.Data;
using Task = INVO20.Data.Task;

namespace INVO20.Pages.Tasks
{
    public class CreateModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public CreateModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["ProjectID"] = new SelectList(_context.Projects, "ID", "Name");
        ViewData["TaskID"] = new SelectList(_context.Tasks, "ID", "Name");
        ViewData["WorkerID"] = new SelectList(_context.Workers, "ID", "Acronimo");
            return Page();
        }

        [BindProperty]
        public Task Task { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Tasks.Add(Task);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
