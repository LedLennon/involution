﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using INVO20.Data;

namespace INVO20.Pages.Tasks
{
    public class DeleteModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public DeleteModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Data.Task Task { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Task = await _context.Tasks
                .Include(t => t.Project)
                .Include(t => t.SuperTask)
                .Include(t => t.Worker).FirstOrDefaultAsync(m => m.ID == id);

            if (Task == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Task = await _context.Tasks.FindAsync(id);

            if (Task != null)
            {
                _context.Tasks.Remove(Task);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
