﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using INVO20.Data;

namespace INVO20.Pages.Workers
{
    public class CreateModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public CreateModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["AreaID"] = new SelectList(_context.Areas, "ID", "Name");
            ViewData["WorkerGroupID"] = new SelectList(_context.WorkersGroups, "ID", "Name");
            return Page();
        }

        [BindProperty]
        public Worker Worker { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Workers.Add(Worker);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
