﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using INVO20.Data;

namespace INVO20.Pages.HourPkts
{
    public class DeleteModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public DeleteModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public HourPkt HourPkt { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HourPkt = await _context.HourPkt
                .Include(h => h.Task)
                .Include(h => h.Worker).FirstOrDefaultAsync(m => m.WorkerID == id);

            if (HourPkt == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HourPkt = await _context.HourPkt.FindAsync(id);

            if (HourPkt != null)
            {
                _context.HourPkt.Remove(HourPkt);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
