﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using INVO20.Data;

namespace INVO20.Pages.HourPkts
{
    public class IndexModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public IndexModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<HourPkt> HourPkt { get;set; }

        public async System.Threading.Tasks.Task OnGetAsync()
        {
            HourPkt = await _context.HourPkt
                .Include(h => h.Task)
                .Include(h => h.Worker).ToListAsync();
        }
    }
}
