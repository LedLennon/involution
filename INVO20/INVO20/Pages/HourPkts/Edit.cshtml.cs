﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using INVO20.Data;

namespace INVO20.Pages.HourPkts
{
    public class EditModel : PageModel
    {
        private readonly INVO20.Data.ApplicationDbContext _context;

        public EditModel(INVO20.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public HourPkt HourPkt { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HourPkt = await _context.HourPkt
                .Include(h => h.Task)
                .Include(h => h.Worker).FirstOrDefaultAsync(m => m.WorkerID == id);

            if (HourPkt == null)
            {
                return NotFound();
            }
           ViewData["TaskID"] = new SelectList(_context.Tasks, "ID", "Name");
           ViewData["WorkerID"] = new SelectList(_context.Workers, "ID", "Acronimo");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(HourPkt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HourPktExists(HourPkt.WorkerID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool HourPktExists(int id)
        {
            return _context.HourPkt.Any(e => e.WorkerID == id);
        }
    }
}
