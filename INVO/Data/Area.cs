﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace INVO.Data
{
    public class Area
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ID { get; set; }

        [MaxLength(32)]
        [Display(Name = "Nome Area")]
        [Required]
        public string Name { get; set; }
    }
}
