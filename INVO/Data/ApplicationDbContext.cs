﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace INVO.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Worker> Workers { get; set; }
        public DbSet<WorkersGroup> WorkersGroups { get; set; }
        public DbSet<HourPkt> HourPkts { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlockDepend>()
                .HasKey(bc => new { bc.BlockId, bc.DependsId });
            modelBuilder.Entity<BlockDepend>()
                .HasOne(bc => bc.Blocks)
                .WithMany(b => b.Blocks)
                .HasForeignKey(bc => bc.BlockId);
            modelBuilder.Entity<BlockDepend>()
                .HasOne(bc => bc.DependsOn)
                .WithMany(c => c.DependsOn)
                .HasForeignKey(bc => bc.DependsId);
        }
    }
}
